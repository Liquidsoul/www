+++
date = "2016-09-04T20:01:41+02:00"
title = "Qui sommes-nous ?"
no_date = true
+++

## Une coopérative d’indépendant·e·s.

Choisir le temps de travail et les missions nous semble indispensable. Cela est à première vue incompatible avec un statut de salarié·e. Cependant, la protection sociale des indépendant·e·s est compliquée à obtenir, et ne nous semble pas offrir autant de garanties que le régime général.

En nous salariant dans une structure commune, nous pouvons être affilié·e·s au régime général, tout en gardant la liberté de notre temps de travail et de nos missions.

## Plus solidaires que solitaires.

Travailler seul·e, c’est magnifique – mais pour beaucoup d’entre nous, c’est un peu usant à la longue.

Au delà d’une simple structure administrative, Codeurs en Liberté est aussi un espace pour travailler ensemble, s’entraider, partager des connaissances et des GIF animés, et se serrer les coudes en cas de coup dur.

## Un fonctionnement militant.

Chaque Codeur en Liberté attache une grande importance au travail fait avec amour. Ainsi, chacun peut travailler moins pour travailler mieux pour garantir la qualité de ses contributions et un équilibre avec la vie privée.

De plus, nous exigeons que chaque membre s’approprie l’entreprise. Cela passe par un actionnariat obligatoire et — en reprenant les principes des coopératives — la possession d’une voix quel que soit le montant de capital détenu.

## Partager nos connaissances.

Nous sommes convaincus que la transparence doit être l’approche par défaut et qu’elle est nécessaire pour permettre l’implication réelle de chaque membre. Nous poussons cette transparence en rendant public la majeure partie de notre activité. Seules les négociations commerciales et les données à caractère personnel ne sont pas publiées.

Par exemple, les décisions de nos assemblées générales, ou encore les échanges sur divers points de fonctionnement (et pas que le résultat final) peuvent être consultés sur https://gitlab.com/CodeursEnLiberte/fondations/.

Nous essayons de faire un grand effort pour documenter nos réalisations et permettre leur réutilisation. Par ailleurs, malgré des membres et des clients aux langues maternelles variées, nous nous efforçons de ne pas retomber systématiquement sur l’anglais comme choix par défaut. 

# Les membres

Tous élevé·e·s en plein air et nourri·e·s au grain (parfois fermenté).

<section class="bios">

{{% bio name="KheOps" pic="kheops.jpg" id="kheops" %}}
{{< hearts "Données personnelles et vie privée, hébergement sécurisé pour petites organisations, audits et formations en sécurité" >}}
{{< email kheops >}}
{{< gpg "D1F546E378CCFAB67B21C79ABA5B6E9F53BB2174" "kheops@ceops.eu.asc" >}}
{{< github kheops2713 >}}
{{< twitter kheops2713 >}}
{{% /bio %}}

{{% bio name="Tristram Gräbener" pic="tristram.jpg" id="tristram" %}}
{{< hearts "Cartographie, transports, données ouvertes" >}}
{{< email tristram >}}
{{< gpg "0B38D6BDD015EBB5B282D0C9483363EE7D669C39" "tristram.asc" >}}
{{< github tristramg >}}
{{< twitter tristramg >}}
{{% /bio %}}


{{% bio name="Pierre de La Morinerie" pic="pierre.jpg" id="pierre" %}}
{{< hearts "Ember.js, Ruby on Rails, le web décentralisé" >}}
{{< email pierre >}}
{{< gpg "337BA619062602F269F47F814CD963CFB150E1E6" "pierre.asc" >}}
{{< github kemenaran >}}
{{< twitter pmorinerie >}}
{{% /bio %}}


{{% bio name="Vincent Lara" pic="vincent.jpg" id="vincent" %}}
{{< hearts "API, transport, backend" >}}
{{< email vincent >}}
{{< gpg "2CECB79AB73ADE9450B740B33B78EAB3740DFDAA" "vincent.asc" >}}
{{< github l-vincent-l >}}
{{< twitter VincentLara >}}
{{% /bio %}}


{{% bio name="Nicolas Bouilleaud" pic="nicolas.jpg" id="nicolas" %}}
{{< hearts "Les utilisateurs, l’accessibilité, les interfaces claires, les transports publics, les cartes et la mobilité" >}}
{{< email nicolas >}}
{{< gpg "7986EEEE813507D0F9ABC21E87FD59575246035A" "nicolas.asc" >}}
{{< github n-b >}}
{{< twitter _nb >}}
{{% /bio %}}


{{% bio name="Thimy Kieu" pic="thimy.jpg" id="thimy" %}}
{{< hearts "Imaginer et construire de belles choses" >}}
{{< email thimy >}}
{{< gpg "04E026A9C851551E35C517163B2FEC816C639F18" "thimy.asc" >}}
{{< github thimy >}}
{{< twitter riddellzz >}}
{{% /bio %}}

{{% bio name="Francis Chabouis" pic="francis.jpg" id="francis" %}}
{{< hearts "Les cartes et les maths" >}}
{{< email francis >}}
{{< gpg "A79356BF9C4A0CE196F78783F81D431138F71D3F" "francis.asc" >}}
{{< github fchabouis >}}
{{% /bio %}}

</section>
